//
//  OnboardingVCSpec.swift
//  EffectiveSwiftTests
//
//  Created by Bogdan Deshko on 9/29/19.
//  Copyright © 2019 Bogdan Deshko. All rights reserved.
//

import Foundation
import Quick
import Nimble
@testable import EffectiveSwift

class OnboardingVCSpec: QuickSpec {
  override func spec() {
    describe("Obvoarding View Controller") {
      it("Should be created") {
        let vc = UIStoryboard(name: OnboardingVC.className,
                              bundle: Bundle(for: OnboardingVC.self))
          // swiftlint:disable:next force_cast
          .instantiateInitialViewController() as! OnboardingVC
        vc.beginAppearanceTransition(true, animated: false)
        vc.endAppearanceTransition()
        expect(vc.firstNameTextField).notTo(beNil())
      }
    }
  }
}
