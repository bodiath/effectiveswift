//
//  ViewController.swift
//  EffectiveSwift
//
//  Created by Bogdan Deshko on 9/29/19.
//  Copyright © 2019 Bogdan Deshko. All rights reserved.
//

import UIKit
import ReaktiveBoar
import ReactiveKit

class OnboardingVC: UIViewController {
  @IBOutlet var firstNameTextField: UITextField!
  @IBOutlet var lastNameTextField: UITextField!

  override func viewDidLoad() {
    super.viewDidLoad()
  }
}

extension OnboardingVC: VCView {

    typealias VMType = OnboardingVM

    func advise(vm: OnboardingVM, bag: DisposeBag) {
        vm.firstName
            .bidirectionalMap(to: { $0 }, from: { $0 ?? "" })
            .bidirectionalBind(to: firstNameTextField.reactive.text)
            .dispose(in: bag)
        vm.lastName
            .bidirectionalMap(to: { $0 }, from: { $0 ?? "" })
            .bidirectionalBind(to: lastNameTextField.reactive.text)
            .dispose(in: bag)
    }
}
