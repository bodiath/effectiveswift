//
//  OnboardingVM.swift
//  EffectiveSwift
//
//  Created by Bogdan Deshko on 9/29/19.
//  Copyright © 2019 Bogdan Deshko. All rights reserved.
//

import Foundation
import ReaktiveBoar
import Swinject
import SwinjectAutoregistration
import ReactiveKit
import Bond

class OnboardingVM: VCVM {

    let firstName = Property("")
    let lastName = Property("")
}
