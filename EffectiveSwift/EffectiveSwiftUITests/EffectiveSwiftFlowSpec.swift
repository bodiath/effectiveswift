//
//  EffectiveSwiftFlowSpec.swift
//  EffectiveSwiftFlowSpec
//
//  Created by Bogdan Deshko on 9/29/19.
//  Copyright © 2019 Bogdan Deshko. All rights reserved.
//

import Foundation
import Quick
import Nimble

class EffectiveSwiftFlowSpec: QuickSpec {
  override func spec() {
    describe("Effective Swift Happy PAth") {
      it("Should Pass") {
        let app = XCUIApplication()
        app.launch()
        app.textFields["firstNameTextField"].tap()
        app.textFields["firstNameTextField"].typeText("Super Name")

        app.textFields["lastNameTextField"].tap()
        app.textFields["lastNameTextField"].typeText("Super Surname")
        expect(app.textFields["firstNameTextField"].text) == "Super Name"
        expect(app.textFields["lastNameTextField"].text) == "Super Surname"

        //sleep(50)
      }
    }
  }
}

extension XCUIElement {
  var text: String? {
    return value as? String
  }
}
//class RecordTests: XCTestCase {
//  override func setUp() {
//    XCUIApplication().launch()
//    continueAfterFailure = true
//  }
//  func testRecord() {
//
//
//  }
//}
